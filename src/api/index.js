import * as trademark from './product/trademark'

import *as user from './acl/user'
import role from './acl/role'
import permission from './acl/permission'

export default {
    trademark,
    user,
    role,
    permission
}