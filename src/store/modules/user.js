import { login, logout, getInfo } from '@/api/acl/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    // 用户名
    name: '',
    // 用户头像
    avatar: '',
    // 职位
    roles:[],
    // 按钮标记
    buttons:[],
    // 异步路由的标记
    routes:[],
    // 不用用户将来计算完毕的全部路由
    userAllRoutes:[]
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  // 存储token
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  // 存储用户所有信息
  SET_USERINFO:(state,data)=>{
    // 存储用户名
    state.name= data.name;
    // 存储头像
    state.avatar = data.avatar;
    // 存储用户职位
    state.roles = data.roles;
    // 存储按钮标记
    state.buttons = data.buttons;
    // 存储异步路由标记
    state.routes = data.routes
  },
  
}

const actions = {
  // user login
  async login({ commit }, userInfo) {

    const { username, password } = userInfo
    // 登录请求
    let result = await login({username:username.trim(),password:password})
    if(result.code === 20000){
      commit('SET_TOKEN', result.data.token)
      setToken(result.data.token)
      return '成功'
    }else{
      return Promise.reject(new Error());
    }
  },

  // 获取用户信息的方法
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response
        commit('SET_USERINFO',data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

