//当前判断此用户有没有这个按钮--封装一个函数
import store from "@/store";


//对外暴露一个函数:判断此用户有没有这个按钮
export const isShowBtn = (tag)=>{
    return store.state.user.buttons.indexOf(tag)!=-1;
}