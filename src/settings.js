module.exports = {

  title: '后台系统',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  // 设置nav导航是否固定
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  // 侧边栏logo的展示
  sidebarLogo: true
}
